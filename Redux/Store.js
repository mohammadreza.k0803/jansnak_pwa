import { createStore } from "redux";

import { Reducer } from "./user/Reducer";
const Store = createStore(Reducer);

export default Store;
