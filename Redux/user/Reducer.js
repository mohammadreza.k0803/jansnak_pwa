import { USER_INFORMATION , RESEND_CODE } from "./Type";
const initialstate = {
  user: [],
};
export const Reducer = (state = initialstate, action) => {
  switch (action.type) {
    case USER_INFORMATION:
      return {
        userinfo: action.payload,
        user: action.user,
      };
      case RESEND_CODE:
      return {
        userinfo: action.payload,
      };
    default:
      return state;
  }
};
