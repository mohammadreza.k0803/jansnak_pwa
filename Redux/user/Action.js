import { USER_INFORMATION, RESEND_CODE } from "./Type";

export const userinformation = (obj, user) => {
  return {
    type: USER_INFORMATION,
    payload: obj,
    user,
  };
};

export const resendcode = (obj) => {
  return {
    type: RESEND_CODE,
    payload: obj,
  };
};
