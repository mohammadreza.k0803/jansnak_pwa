import React, { useEffect, useState } from "react";
//import component
import Logo from "./../component/Logo/Logo";
import { Button, Container, Divider } from "@material-ui/core";
import VerificationInput from "react-verification-input";
import Timer from "../component/Timer/Timer";
//import Api
import { resendotp, sendnotification } from "../Api";
//import redux
import { useSelector, useDispatch } from "react-redux";
import { resendcode } from "../Redux/user/Action";
import { useRouter } from "next/router";
//import swal
import Swal from "sweetalert2";
import { encrypt } from "react-crypt-gsm";

const Checkcode = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const numb = useSelector((state) => state.user);
  const data = useSelector((state) => {
    if (state.userinfo) {
      return state.userinfo[0];
    } else {
      return {
        OTP: "لطفا شماره ایی را ثبت کنید",
        User: "لطفا شماره ایی را ثبت کنید",
        UserSecret: "لطفا شماره ایی را ثبت کنید",
        loading: true,
      };
    }
  });
  const user = data;

  const [state, setState] = useState({
    OTP: user.OTP,
    User: numb,
    UserSecret: user.UserSecret,
    loading: true,
  });

  const [parametr] = useState([
    {
      username: user.User,
      UserSecret: user.UserSecret,
    },
  ]);

  const handlesubmit = () => {
    setState({ ...state, loading: false });
    resendotp(parametr)
      .then((res) => {
        setState({ ...state, loading: true });
        res.data.map((s) => setState({ ...state, OTP: s.OTP }));
      })
      .catch((err) => {
        setState({ ...state, loading: true }), console.log(err);
        return Swal.fire({
          position: "center",
          icon: "warning",
          title: "شماره را ثبت کنید",
          showConfirmButton: false,
          timer: 2000,
        });
      });
  };
  const sendnotihandle = () => {
    sendnotification(parametr)
      .then((res) => {
        return Swal.fire({
          position: "center",
          icon: "success",
          title: "نوتیفیکیشن ارسال شد",
          showConfirmButton: false,
          timer: 2000,
        });
      })
      .catch((err) => {
        console.log(err);
        return Swal.fire({
          position: "center",
          icon: "warning",
          title: "شماره را ثبت کنید",
          showConfirmButton: false,
          timer: 2000,
        });
      });
  };

  return (
    <Container maxWidth="lg">
      <div className="body-flex">
        <div className="top-div">
          <Logo />
        </div>
        <div className="mid-div1 ">
          <h3>تغییر otp</h3>
        </div>
        <div className="mid-div2">
          {state.loading ? (
            <>
              <div>OTP:{state.OTP}</div>
              <div>User:{state.User}</div>
              <div>UserSecret:{state.UserSecret}</div>
            </>
          ) : (
            "درحال بارگذاری اطلاعات ..."
          )}

          {/* <div>
            <VerificationInput
              placeholder=""
              removeDefaultStyles
              length={4}
              classNames={{
                container: "container",
                character: "character",
                characterSelected: "character--selected",
              }}
              onChange={handlechange}
            />
          </div> */}
          <div className="flex">
            <Button
              variant="contained"
              color="primary"
              className="field button"
              onClick={handlesubmit}
            >
              رمز جدید
            </Button>
            <Button
              variant="contained"
              color="primary"
              className="field button"
              onClick={sendnotihandle}
            >
              ارسال نوتیفیکیشن
            </Button>
            <Button
              variant="contained"
              color="primary"
              className="field button"
              onClick={() => router.push("/")}
            >
              ثبت شماره
            </Button>
          </div>
        </div>
        <div className="bottom-div">
          {/* <Divider className="width-divider" /> */}
          {/* <Timer /> */}
        </div>
      </div>
    </Container>
  );
};

export default Checkcode;
