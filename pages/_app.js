import React, { useEffect, useState } from "react";
//import next
import Head from "next/head";
//import css
import "../styles/globals.css";
//import material
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
//import
import Splash from "../component/Splash/Splash";
//import redux
import { Provider } from "react-redux";
import Store from "../Redux/Store";

const theme = createMuiTheme({
  palette: {
    primary: {
      light: "#63a4ff",
      main: "#ef7c01",
      dark: "#ef7c01",
      contrastText: "#fff",
    },
    secondary: {
      light: "#ffffff",
      main: "#fafafa",
      dark: "#ECECEC",
      contrastText: "#212121",
    },
  },
});
function MyApp({ Component, pageProps }) {
  const [state, setState] = useState({
    welcome: true,
  });
  useEffect(() => {
    setTimeout(() => {
      setState({ welcome: false });
    }, 2000);
  }, []);
  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta
          name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"
        />
        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />
        <title>جان اسنک</title>

        <link rel="manifest" href="/manifest.json" />
        <link
          href="/icons/icon-16x16.png"
          rel="icon"
          type="image/png"
          sizes="16x16"
        />
        <link
          href="/icons/icon-32x32.png"
          rel="icon"
          type="image/png"
          sizes="32x32"
        />
        <link rel="apple-touch-icon" href="/apple-icon.png"></link>
        <meta name="theme-color" content="#004ba0" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/cryptojs/3.1.2/rollups/aes.js"></script>
      </Head>
      {state.welcome == true ? (
        <Provider store={Store}>
          <Splash />
        </Provider>
      ) : (
        <Provider store={Store}>
          <ThemeProvider theme={theme}>
            <Component {...pageProps} />
          </ThemeProvider>
        </Provider>
      )}
    </>
  );
}

export default MyApp;
