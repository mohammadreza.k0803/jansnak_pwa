import React, { useState } from "react";
//import component
import Logo from "../component/Logo/Logo";
import {
  Button,
  Container,
  Divider,
  CircularProgress,
} from "@material-ui/core";
import StayCurrentPortraitIcon from "@material-ui/icons/StayCurrentPortrait";
//import api
import { sendcode } from "../Api";
//import swal
import Swal from "sweetalert2";
//import router
import { useRouter } from "next/router";
//import redux
import { useDispatch } from "react-redux";
import { userinformation } from "../Redux/user/Action";

const Sendcode = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const [state, setState] = useState({
    username: String,
  });
  const [loading, setLoading] = useState(false);
  const handlechange = (e) => {
    setState({ ...state, username: e.target.value });
  };

  const handlesubmit = () => {
    if (state.username.length <= 10) {
      setState("");
      return Swal.fire({
        position: "center",
        icon: "warning",
        title: "شماره را به درستی وارد کنید",
        showConfirmButton: false,
        timer: 2000,
      });
    } else {
      setLoading(true);
      sendcode(state.username)
        .then((res) => {
          console.log(res);
          setLoading(false);
          dispatch(userinformation(res.data, state.username));
          res.status == 200
            ? router.push("/checkcode")
            : Swal.fire({
                position: "center",
                icon: "error",
                title: "خطا",
                showConfirmButton: false,
                timer: 2000,
              });
        })
        .catch((err) => {
          setLoading(false);
          Swal.fire({
            position: "center",
            icon: "error",
            title: "خطا",
            showConfirmButton: false,
            timer: 2000,
          });
        });
    }
  };
  return (
    <Container maxWidth="lg">
      <div className="body-flex">
        <div className="top-div">
          <Logo />
        </div>
        <div className="mid-div1 ">
          <h3>خوش آمدید</h3>
          <p className="jus-text">
            برای استفاده از خدمات اپلیکیشن جان اسنک لازم است ابتدا به حساب
            کاربری خود وارد شوید
          </p>
        </div>
        <div className="mid-div2">
          <div className="input">
            <div>
              <div className="icon-input">
                <StayCurrentPortraitIcon />
              </div>
            </div>
            <Divider
              orientation="vertical"
              style={{ width: "1px", backgroundColor: "white" }}
            />
            <div>
              <input
                type="tel"
                required
                className="input-hide"
                placeholder="09xx xxx xxxx"
                onChange={handlechange}
              />
            </div>
          </div>
          <div>
            <Button
              variant="contained"
              color="primary"
              className="field"
              onClick={handlesubmit}
            >
              {loading ? (
                <CircularProgress color="secondary" size={30} />
              ) : (
                "ورود به جان اسنک"
              )}
            </Button>
          </div>
        </div>
        <div className="bottom-div">
          <Divider className="width-divider" />
          <p className="jus-text">
            با ورود یا ثبت نام در جان اسنک شما
            <a href="#"> شرایط و قوانین </a>استفاده از سرویس های اپلیکیشن و سایت
            جان اسنک را می‌پذیرید
          </p>
        </div>
      </div>
    </Container>
  );
};

export default Sendcode;
