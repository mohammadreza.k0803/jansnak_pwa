import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { Container } from "@material-ui/core";
import ItemList from "../List/ItemList";
import Account from "../Account/Account";
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
      className="tabpanel"
    >
      {value === index && (
        <Box>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function SimpleTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" style={{ backgroundColor: "#fafafa" }}>
        <Container maxWidth="lg">
          <Tabs
            value={value}
            onChange={handleChange}
            aria-label="simple tabs example"
            indicatorColor="primary"
            textColor="primary"
          >
            <Tab
              className="tabpanel-font"
              label="طرح کسب و کار"
              {...a11yProps(0)}
            />
            <Tab
              className="tabpanel-font"
              label="اطلاعات مالی"
              {...a11yProps(1)}
            />
            <Tab className="tabpanel-font" label="تیم" {...a11yProps(2)} />
            <Tab className="tabpanel-font" label="نظرات" {...a11yProps(3)} />
          </Tabs>
        </Container>
      </AppBar>
      <Container maxWidth="lg">
        <TabPanel value={value} index={0}>
          <p className="jus-text">
            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
            استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در
            ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز،
            و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای
            زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و
            متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان
            رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد
            کرد، در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه
            راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل
            حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود
            طراحی اساسا مورد استفاده قرار گیرد.
          </p>
        </TabPanel>
        <TabPanel value={value} index={1}>
          <ItemList
            title="مبلغ مورد نیاز سرمایه"
            content="100000میلیون تومان"
          />
          <ItemList title="مبلغ مورد نیاز در ماه" content="100میلیون تومان" />
        </TabPanel>
        <TabPanel value={value} index={2}>
          <Account />
          <Account />
          <Account />
          <Account />
          <Account />
          <Account />
          <Account />
          <Account />
          <Account />
          <Account />
          <Account />
          <Account />
        </TabPanel>
        <TabPanel value={value} index={3}>
          <p className="jus-text">
            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
            استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله
          </p>
        </TabPanel>
      </Container>
    </div>
  );
}
