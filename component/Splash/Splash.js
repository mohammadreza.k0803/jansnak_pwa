import React, { useEffect, useState } from "react";
//import component
import Logo from "../Logo/Logo";
import Version from "../Version/Version";
//import material
import Divider from "@material-ui/core/Divider";
const Splash = () => {
  return (
    <div className="body-splash">
      <div>
        <Logo />
      </div>
      <div className="version">
        <Divider />
        <Version />
      </div>
    </div>
  );
};

export default Splash;
