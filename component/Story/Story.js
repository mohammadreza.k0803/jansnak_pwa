import React from "react";

//import component
import Avatar from "./Avatar/Avatar";
//import material ui
import { Container } from "@material-ui/core";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/swiper.min.css";
import "swiper/components/pagination/pagination.min.css";

const Story = () => {
  return (
    <div className="story">
      <Container maxWidth="lg">
        <div className="story-main">
          <Swiper
            freeMode={true}
            breakpoints={{
              320: {
                slidesPerView: 4,
                spaceBetween: 20,
              },
              480: {
                slidesPerView: 5,
                spaceBetween: 20,
              },
              900: {
                slidesPerView: 8,
                spaceBetween: 30,
              },
              1200: {
                slidesPerView: 10,
                spaceBetween: 40,
              },
            }}
          >
            <SwiperSlide>
              <Avatar name="میلاد جلالی" person={true} />
            </SwiperSlide>
            <SwiperSlide>
              <Avatar name="علی رادمنش" story="1" />
            </SwiperSlide>
            <SwiperSlide>
              <Avatar name="محمد حسنی" story="1" />
            </SwiperSlide>
            <SwiperSlide>
              <Avatar name="سینا محمدی" story="1" />
            </SwiperSlide>
            <SwiperSlide>
              <Avatar name="فاطمه ذبیحی" story="1" />
            </SwiperSlide>
            <SwiperSlide>
              <Avatar name="محمد رضایی" story="1" />
            </SwiperSlide>
            <SwiperSlide>
              <Avatar name="میلاد جلالی" />
            </SwiperSlide>
            <SwiperSlide>
              <Avatar name="میلاد جلالی" />
            </SwiperSlide>
            <SwiperSlide>
              <Avatar name="میلاد جلالی" />
            </SwiperSlide>
            <SwiperSlide>
              <Avatar name="میلاد جلالی" />
            </SwiperSlide>
            <SwiperSlide>
              <Avatar name="میلاد جلالی" />
            </SwiperSlide>
            <SwiperSlide>
              <Avatar name="میلاد جلالی" />
            </SwiperSlide>
            <SwiperSlide>
              <Avatar name="میلاد جلالی" />
            </SwiperSlide>
            <SwiperSlide>
              <Avatar name="میلاد جلالی" />
            </SwiperSlide>
            <SwiperSlide>
              <Avatar name="میلاد جلالی" />
            </SwiperSlide>
            <SwiperSlide>
              <Avatar name="میلاد جلالی" />
            </SwiperSlide>
            <SwiperSlide>
              <Avatar name="میلاد جلالی" />
            </SwiperSlide>
            <SwiperSlide>
              <Avatar name="میلاد جلالی" />
            </SwiperSlide>
          </Swiper>
        </div>
      </Container>
    </div>
  );
};

export default Story;
