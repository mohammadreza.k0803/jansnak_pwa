import React from "react";
//import next
import Link from "next/link";
import { useRouter } from "next/router";
const ItemFooter = (props) => {
  const router = useRouter();
  const { icon, path, name } = props;
  return (
    <Link href={path}>
      <div
        className="item-footer"
        style={router.pathname == path ? {color:"#004ba0"} : null}
      >
        {icon}
        <p>{name}</p>
      </div>
    </Link>
  );
};

export default ItemFooter;
