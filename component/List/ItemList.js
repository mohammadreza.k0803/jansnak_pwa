import React from "react";
//import material ui
import Divider from "@material-ui/core/Divider";
import { Container } from "@material-ui/core";
const ItemList = (props) => {
  const { title, content } = props;
  return (
    <>
      <Container maxWidth="lg">
        <div className="list">
          <div className="item-list right">
            <h5>{title}</h5>
          </div>
          <div className="item-list left">
            <p>{content}</p>
          </div>
        </div>
        <Divider />
      </Container>
    </>
  );
};

export default ItemList;
