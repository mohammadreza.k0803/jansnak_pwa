import React from "react";
//import material ui
import { Container } from "@material-ui/core";

// Import Swiper styles
import "swiper/swiper.min.css";
import "swiper/components/pagination/pagination.min.css";
// import Swiper core and required modules
import SwiperCore, { Pagination, Autoplay } from "swiper/core";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
// install Swiper modules
SwiperCore.use([Pagination, Autoplay]);
const SliderFunding = () => {
  return (
    <Container maxWidth="lg">
      <Swiper
        style={{ marginBottom: "16px" }}
        spaceBetween={30}
        pagination={{
          clickable: true,
        }}
        className="mySwiper"
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
      >
        <SwiperSlide>
          <img src="https://www.thedigitaltransformationpeople.com/wp-content/uploads/2019/11/Innovation-is-not-about-ideas.jpg" />
        </SwiperSlide>
        <SwiperSlide>
          <img src="https://www.thedigitaltransformationpeople.com/wp-content/uploads/2019/11/Innovation-is-not-about-ideas.jpg" />
        </SwiperSlide>
        <SwiperSlide>
          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQTt049tq-hJTYInUlKa8zmeBUgqSolj2wMBQ&usqp=CAU" />
        </SwiperSlide>
      </Swiper>
    </Container>
  );
};

export default SliderFunding;
