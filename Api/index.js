import axios from "axios";

import { encrypt } from "react-crypt-gsm";
axios.defaults.baseURL = "https://testapi.jansnak.com/api";
axios.defaults.headers.common[
  "Authorization"
] = `Bearer NSc%40_VBsfvs21054874N%40bbhscvbs0dV115Sc_sd2155441Scd`;

axios.defaults.timeout = 10000;

export const sendcode = (a) => {
  const result = [
    {
      username: encrypt(a).content,
    },
  ];

  return axios.post(
    `/ctestotp/RegisterUser?token=NSc%40_VBsfvs21054874N%40bbhscvbs0dV115Sc_sd2155441Scd`,
    result
  );
};
export const sendnotification = (parametr) => {
  return axios.post(
    `/ctestotp/showOTP?token=NSc%40_VBsfvs21054874N%40bbhscvbs0dV115Sc_sd2155441Scd`,
    parametr,
  );
};

export const resendotp = (parametr) => {
  return axios.post(
    `/ctestotp/setOTP?token=NSc%40_VBsfvs21054874N%40bbhscvbs0dV115Sc_sd2155441Scd`,
    parametr
  );
};
